<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m200826_043712_Creacion_tabla_Usuario
 */
class m200826_043712_Creacion_tabla_Usuario extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200826_043712_Creacion_tabla_Usuario cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
         
        $this -> createTable('usuario',[
            'id' => Schema::TYPE_PK,
            'nombre' => Schema::TYPE_STRING . ' NOT NULL',
            'apellido' => Schema::TYPE_STRING . ' NOT NULL',
            'edad' => Schema::TYPE_INTEGER . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'dni' => Schema::TYPE_STRING . ' NOT NULL',
            'amigo' => Schema::TYPE_STRING,
        ]);
       
    }

    public function down()
    {
        echo "m200826_043712_Creacion_tabla_Usuario cannot be reverted.\n";
        $this->dropTable('usuario');
 
        return false;
    }
    
}
