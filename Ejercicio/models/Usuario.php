<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellido
 * @property int $edad
 * @property string $email
 * @property string $dni
 * @property string $amigo
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'edad', 'email', 'dni'], 'required', 'message' => 'El campo {attribute} no puede estar vacio'],
            [['edad'],'integer','message' => 'La {attribute} debe ser un campo numerico', 'min'=>18,'tooSmall' => 'La {attribute} debe ser mayor a 18 años'],
            [['nombre', 'apellido'], 'string', 'max' => 100, 'message' => 'No pueden contener más de 100 caracteres'],
            [['email'], 'email','message' => 'La direccion de correo es invalida'],
            [['dni'], 'integer', 'max' =>100000000, 'message' => 'El {attribute} debe ser un campo numerico','tooBig' => "El {attribute} debe tener como maximo 10 caracteres"],
            [['amigo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'edad' => 'Edad',
            'email' => 'Email',
            'dni' => 'DNI',
            'amigo' => 'Amigo',
        ];
    }
}
